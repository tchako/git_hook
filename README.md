### Standardize commit message and branch name 

Projects will sometime require a conventional branch naming and also a specific structure for your commit message like :
. Branches  name should be  `JIRA-000/feature`
. Commit messages have to be `JIRA-000:minor` 

In this workshop I will show you how to standaridze these things among your team/project
We will make use of `git hooks` especially the `client-side hooks` for our rules.
We will make use of `pre-commit` and `commit-msg` these hooks can be call using git-commit, while commit-msg can also be call using `git-merge`. Both can be by-passed with the --no-verify flag.
We will use these 2 hooks to check the branch name and the commit message.

## Create Hooks directory
Inside your git repository/project create a subdirectory called hooks, with 2 hooks files `pre-commit` `commit-msg`
```bash
mkdir ~/hooks
cd ~/hooks

touch pre-commit commit-msg
chmod +x pre-commit commit-msg
```
## Enforce Branch Name
We want to enforce the branch Name as  `JIRA-0000/feature` , also we don't want to exclude develop, main, release, PROD, QA. Let us create a regular expression that and store it inside a variable `REGEX_TEST`

```bash
REGEX_TEST="^(JIRA-[0-9]+\/([a-ZA-Z0-9]|-)+|develop|master|release|PROD|QA)"
```

We also need to store the branch name and we can easily have it by running the command 
``` bash
git rev-parse --abbrev-ref HEAD

```
with this we are all set. We need a simple script that will check if the Branch respect our convention if no we will echo and error message and exit witha the exit code 1.
now our pre-commit file will look like this
# pre-commit script 
```bash
#!/bin/bash
REGEX_TEST="^(JIRA-[0-9]+\/([a-ZA-Z0-9]|-)+|develop|main|release|PROD|QA)"
BRANCH_NAME_TEST=$(echo $(git rev-parse --abbrev-ref HEAD) | grep -o -E "$REGEX_TEST")

if [[ -z "$BRANCH_NAME_TEST" ]]; then
    echo "[pre-commit-hook] Your branch name doesn't meet the criteria . Please rename your branch using the folowing regex: $REGEX_TEST"
    exit 1
fi

```
## Enforce commit message
With this scenario we will enforce the branch and the commit message using this script
# commit-msg script

```bash
#!/bin/bash

# Define the required commit message prefix
REQUIRED_PREFIX="JIRA-000:minor"

# Get the commit message from the file specified as an argument
COMMIT_MESSAGE=$(cat "$1")

# Check if the commit message starts with the required prefix
if [[ ! "$COMMIT_MESSAGE" =~ ^"$REQUIRED_PREFIX" ]]; then
    echo "[commit-msg-hook] Your commit message must start with '$REQUIRED_PREFIX'."
    exit 1
fi
```
